#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nombres.h"


int main()
{
    nombre *test = (nombre*) NULL;
    int choix, k, x;
    Bool running;
    do{
        x = 0; k = 0; choix = 0, running = true;
        printf("------------Menu--------------\n");
        printf("\n  1 : creer nombre.");
        printf("\n  2 : Afficher la liste.");
        printf("\n  3 : Longueur de la liste.");
        printf("\n  4 : Ajouter un element.");
        printf("\n  5 : Ajouter un element en position k");
        printf("\n  6 : Rechercher un element dans la liste.");
        printf("\n  7 : Suprimer un element de la liste.");
        printf("\n  8 : Trier la liste par ordre croissant.");
        printf("\n  9 : Modifier un element de la liste.");
        printf("\n 10 : Ajouter dans une liste triee.");
        printf("\n--> ");
        scanf("%d", &choix);
        switch(choix){
            case 1 :{
                int *valeurs = NULL;
                k = 0;
                printf("\nEntrer -1 pour passer le test de fin.");
                while(running){
                    printf("\nEntrer un nombre : ");
                    scanf("%d", &x);
                    if(x == -1) running = continuer(false, false);
                    if(running){
                        valeurs = realloc(valeurs, (k + 1) * sizeof(int));
                        valeurs[k] = x;
                        k++;
                    }
                }
                test = creer_nombres(valeurs, k);
                done();
                break;

            }case 2 :{
                afficher_nombres(test);
                break;

            }case 3 :{
                printf("\nla longueur de la liste est : %d",longueur(test));
                break;

            }case 4 :{
                Ajouter(&test);
                break;

            }case 5:{
                printf("\nEntrez la position : ");
                scanf("%d", &x);
                AjouterPos(&test, x);
                break;

            }case 6:{
                printf("\nEntrez le nombre que vous recherche : ");
                scanf("%d", &x);
                Bool result = Rechercher(test, x, &k);
                printf("\n%d%sest dans la liste", x, (result ? " " : " n'"));
                if (result) printf(" en position %d", k);
                break;

            } case 7:{
                printf("\nEntrez le nombre a effacer : ");
                scanf("%d", &x);
                Supprimer(&test, x);
                break;

            } case 8:{
                triDeListe(test);
                break;

            } case 9:{
                printf("\nEntrez le nombre a remplacer : ");
                scanf("%d", &x);
                printf("\nEntrez le nombre remplacant : ");
                scanf("%d", &k);
                modifier(test, x, k);
                break;

            } case 10:{
                ajout_bonne_endroit(test);
                break;
            }
        }
        fflush(stdin);
    } while(continuer(true, true));

    return 0;
}
