#ifndef NOMBRES_H_INCLUDED
#define NOMBRES_H_INCLUDED


#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <ctype.h>


    //d�claration de la structure nombre
    typedef struct nombre {
        int nombre_valeur;
        struct nombre *suivant;
    }nombre ;

    nombre* liste = NULL;
//-----------------------------------------------------------------------------
    //Implementation du type booleen
    typedef enum {
        false,
        true
    }Bool;

//-----------------------------------------------------------------------------
    //Fonction pour continuer ou stopper le programme.
    Bool continuer();
    //fonction d'attestationd de fin
    void done();
    //fonction qui permet de construire une liste
    nombre* creer_nombres(int x[], int length);
    //fonction d'affichage
    void afficher_nombres();
    //Fonction de calcul de la longueur
    int longueur(nombre* liste);
    //fonction qui permet d'ajouter un enregistrement dans la liste chainée
    void Ajouter(nombre* *element);
    //fonction qui permet d'ajouter un enregistrement dans la liste chainée à la position pos
    void AjouterPos(nombre* *c, int pos);
    //fonction recherchant un nombre dans la liste.
    Bool Rechercher(nombre* liste, int x, int *pos);
    //fonction de suppression
    void Supprimer(nombre* *element, int valeur);
    //Fonction de tri
    void triDeListe(nombre* liste) ;
    //Modifier un nombre de le liste
    void modifier(nombre *liste, int ancien, int nouvelle);
    //Fonction d'ajout au bonne endroit après le tri
    void ajout_bonne_endroit(nombre* liste) ;


//-----------------------------------------------------------------------------

    Bool continuer(Bool clean, Bool program){
        char reponse;
        do{
            printf("\nContinuer%s? (y pour oui et n pour non) : ", (program) ? " le programme" : " ");
            reponse=tolower(getche());
            if(clean) system("CLS");
        }while(reponse!='y' && reponse!='n');
        return (reponse == 'y') ? true : false;
    }

    void done(){
        printf("\nOperation effectuer avec succes.");
    }

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

    /* --------------------------- code 1 --------------------------- */

    nombre* creer_nombres(int *x, int length) {
		nombre *element;
		nombre *liste = malloc(sizeof(*element));
		liste = NULL;
		for (int k = 0; k < length; k++){
            element = malloc(sizeof(*element));
            if(element == NULL) {
                exit(EXIT_FAILURE);
            }
            element->nombre_valeur = x[k];
            element->suivant = NULL;
            if(liste == NULL){
                liste = element;
                continue;
            }
            nombre *temp;
            temp = liste;
            while(temp -> suivant != NULL){
                temp = temp -> suivant;
            }
            temp -> suivant = element;
		}
		return liste;
	}

    /* --------------------------- code 2 --------------------------- */

	void afficher_nombres(nombre *liste) {
		if (liste == NULL) {
			printf ("\nLa liste est vide.");
			return;
		}

		nombre *courant = liste;
		while(courant != NULL) {
			printf ("[%d] ", courant->nombre_valeur);
			courant = courant ->suivant;
		}
		printf("\n");
	}

    /* --------------------------- code 3 --------------------------- */

    int longueur(nombre* liste){
        nombre* temp = liste ;

        int longueur = 0 ;
        if(temp == NULL )
            printf("\nLa liste est vide") ;
        while(temp != NULL){
            longueur++ ;
            temp = temp->suivant ;
        }
        return longueur ;
    }

    /* --------------------------- code 4 --------------------------- */

    void Ajouter(nombre* *element)
    {
        nombre *nouveauElement;
        nouveauElement = (nombre*)malloc(sizeof(struct nombre));
        int donnee;

        printf("\nVeuillez saisir l'element a ajouter : ") ;
        scanf("%d",&donnee) ;

        nouveauElement->nombre_valeur = donnee;

        if(nouveauElement == NULL)
        {
            fprintf(stderr, "\nErreur : probleme allocation dynamique.\n");
            exit(EXIT_FAILURE);
        }


        if((*element) == NULL)
             nouveauElement->suivant = NULL;
        else
             nouveauElement->suivant = (*element);

        (*element) = nouveauElement;
        done();
    }

    /* --------------------------- code 5 --------------------------- */

    void AjouterPos(nombre* *c, int pos)
    {
        nombre *temp = *c;
        int nombre_element = longueur(temp);

        if((nombre_element + 1) < pos)
        {
            printf("\n\t\t\tla position indiquee n\'est pas permise ! Le nombre d'elements actuel est %d", nombre_element);
            return;
        }

        int k = 1, donnee;
        nombre *p, *q, *nouveauElement;
        nouveauElement = (nombre*)malloc(sizeof(struct nombre));

        printf("\nVeuillez saisir l'element a ajouter : ") ;
        scanf("%d",&donnee) ;

        nouveauElement->nombre_valeur = donnee;

        p = (*c);

        //Insertion au début
        if(pos == 1)
        {
            nouveauElement->suivant = p;
            (*c) = nouveauElement;
        }
        else
        {
            // On parcours la liste jusqu'à la position désirée
            while((p != NULL) && (k<pos))
            {
                k++;
                q = p;
                p = p->suivant;
            }
            q->suivant = nouveauElement;
            nouveauElement->suivant = p;
            done();
        }
    }

    /* --------------------------- code 6 --------------------------- */

    Bool Rechercher(nombre* liste, int x, int *pos) {
        nombre* courant = liste;

        while (courant != NULL) {
            (*pos) ++;
            if (courant->nombre_valeur == x) {
                return true;
            }
            courant = courant->suivant;
        }

        return false;
    }

    /* --------------------------- code 7 --------------------------- */

    void Supprimer(nombre* *liste, int k){
        if ((*liste) == NULL){
            printf("\nListe Vide");
        }else{
            nombre *queue = *liste;
            if((*liste)->nombre_valeur == k){
                *liste = (*liste)->suivant;
                free(queue);
                return;
            }
            nombre *element = (*liste)->suivant;
            nombre *temp;
            while(element != NULL){
                if(element->nombre_valeur == k){
                    temp = element;
                    queue->suivant = element->suivant;
                    free(temp);
                    done();
                    return;
                }
                queue = element;
                element = element->suivant;
            }
        }
    }

    /* --------------------------- code 8 --------------------------- */

    void triDeListe(nombre* liste) {
        nombre *courant = liste;
        nombre *index = NULL;
        int temp;

        if(liste == NULL) {
            printf("\nListe vide");
        }
        else {
            while(courant != NULL) {
                index = courant->suivant;

                while(index != NULL) {

                    if(courant->nombre_valeur > index->nombre_valeur) {
                        temp = courant->nombre_valeur;
                        courant->nombre_valeur = index->nombre_valeur;
                        index->nombre_valeur = temp;
                    }
                    index = index->suivant;
                }
                courant = courant->suivant;
            }
            done();
        }
    }

    /* --------------------------- code 9 --------------------------- */

    void modifier(nombre *liste, int ancien, int nouvelle){
        if (liste == NULL){
            printf("\nListe Vide");
        }else{
            nombre *element = liste;
            while (element != NULL) {
                if (element->nombre_valeur == ancien) {
                    element->nombre_valeur = nouvelle;
                    done();
                    return;
                }
                element = element->suivant;
            }
            printf("element non trouve.");
        }
    }

    /* --------------------------- code 10 -------------------------- */

    void ajout_bonne_endroit(nombre* liste){

        if(liste == NULL ){
            printf("\nla liste est vide") ;
            return ;
        }

        printf("operation de tri...");
        triDeListe(liste);

        nombre *nouveauelement ,*avant ,*temp ;
        temp = liste ;
        int donnee ;

        nouveauelement = (nombre*)malloc(sizeof(*nouveauelement)) ;
        if(nouveauelement == NULL)
        {
            fprintf(stderr, "\nErreur : probleme d'allocation.\n");
            exit(EXIT_FAILURE);
        }


        printf("\nveuillez saisir l'element à ajouter : ") ;
        scanf("%d", &donnee) ;
        nouveauelement->nombre_valeur = donnee ;

        while(temp != NULL){
            if (donnee <= temp->nombre_valeur) break;
            avant = temp ;
            temp = temp->suivant ;
        }

        avant->suivant = nouveauelement ;
        nouveauelement->suivant = temp ;
        done();
    }

#endif // NOMBRES_H_INCLUDED
